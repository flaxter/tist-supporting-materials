library(kernlab)

default.kernel = function(x) {
  dists = as.vector(dist(x))
  sig = median(dists)
  return(rbfdot(sigma=1/(sig^2)))
}
hsicTestGamma.wrapper = function(X, Y, kernX = default.kernel(X), kernY = default.kernel(Y)) {
  K = kernelMatrix(kernX,X)
  L = kernelMatrix(kernY,Y)
  (hsicTestGamma(K,L))
}

# based on matlab code by Arthur Gretton (hsicTestGamma.m, 03/06/07):
# http://www.gatsby.ucl.ac.uk/~gretton/indepTestFiles/indep.htm
hsicTestGamma = function(K, L, Kcentered=FALSE) {
  m = nrow(K)
  bone = rep(1,m)
  H = diag(m)-matrix(rep(1/m,m*m),nrow=m,ncol=m)
  if(Kcentered) {
    Kc = K
  } else {
    Kc = H%*%K%*%H
  }
  Lc = H%*%L%*%H
  
  testStat = 1/m * sum(t(Kc)*Lc)
  
  varHSIC = (1/6 * Kc*Lc)^2
  
  varHSIC = 1/m/(m-1)* (  sum(sum(varHSIC)) - sum(diag(varHSIC))  )
  varHSIC = 72*(m-4)*(m-5)/m/(m-1)/(m-2)/(m-3)  *  varHSIC
  diag(K) = 0
  diag(L) = 0
  
  muX = 1/m/(m-1)*t(bone) %*% (K %*% bone)
  muY = 1/m/(m-1)*t(bone) %*% (L %*% bone)
  mHSIC  = 1/m * ( 1 +muX %*% muY  - muX - muY )
  
  al = mHSIC^2 / varHSIC
  bet = varHSIC*m / mHSIC
  
  return(pgamma(testStat, al, scale=bet,lower=FALSE))
}


check.cond.indep = function(X,Y,Z, suffStat=NULL, sigma=1) {
  result1 = prewhiten(as.matrix(Z), as.matrix(Y), sigma=sigma) # was gp_regression.scholkopf
  result2 = prewhiten(as.matrix(Z), as.matrix(X), sigma=sigma) # was gp_regression.scholkopf
  return(hsicTestGamma.wrapper(result1$residuals, result2$residuals))
}

check.cond.indep.PC <- function(x, y, S, params) {
  cat("ciTest called with ", x, " ", y, " given ", S)
  if(length(S) == 0) {
    p.value = hsicTestGamma.wrapper(params$data[,x], params$data[,y])
    cat("nothing. result: ", p.value, "\n")
    return(p.value)
  } else {
    p.value = check.cond.indep(params$data[,x], params$data[,y], params$data[,S], params$sigma)
    cat(" result: ", p.value, "\n")
    return(p.value)
  } 
}

prewhiten = function(S,x, kS=default.kernel(S), sigma=1) {
  fit = gausspr(S, x, fit=FALSE, variance.model=TRUE, var=sigma^2, kernel=kS, scaled=FALSE)
  xhat = predict(fit,S)
  return(list(fit=fit, xhat=xhat, residuals=xhat - x))
}

plot.graph = function(g, labels) {
  library(Rgraphviz)
  nodes(g) = labels
  fontsize <- rep(1, (length(labels)))
  nAttrs <- list()
  nAttrs$width <-  rep(1, length(labels))
  names(nAttrs$width)<-labels
  nAttrs$height <- rep(1, length(labels))
  names(nAttrs$height)<-labels
  
  names(fontsize)<-labels
  x <- layoutGraph(g, nodeAttr=nAttrs)
  nodeRenderInfo(x) <- list(cex=fontsize)
  renderGraph(x) 
}
