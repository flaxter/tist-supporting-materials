## --> Step 1: ozone.data.r loads the data, standardizes it (so each variable has mean 0 and variance 1), and saves it for matlab
## Step 2: ozone.m pre-whitens each variable
## Step 3: ozone.r runs the PC algorithm and makes plots

## The classical ozone dataset (Breiman and Friedman 1985)
## appears in various formats in various packages in R.
## Most importantly, we need the day of the year field, since we care
## about correcting for temporal autocorrelation.

library(R.matlab)
library(gclus)
library(hcc)

data(ozone, package="gclus")
head(ozone) # missing day of year!
data = ozone
for(i in 1:ncol(data)) {
  data[,i] = scale(data[,i])
}
save(data, file="ozone.rdata")

data(ozone, package="hcc")
head(ozone)
cor(ozone$vis,data$Vis) # okay, they're the same

writeMat("ozone.mat", data=cbind(S=as.numeric(ozone$doy),data))
