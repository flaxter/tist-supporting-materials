Have you ever wanted to test for associations between variables that are observed in space, time, or on a network? Even better, have you ever wanted to perform algorithmic causal inference with a spatiotemporal dataset?

This repository contains all of the code required to reproduce the results in:

Flaxman, Neill, and Smola, "Gaussian Processes for Independence Tests with non-iid Data in Causal Inference," ACM TIST, 2015.

Dependencies: 

- matlab: GPstuff, GPML
- R: kernlab, pcalg

Author: Seth Flaxman <flaxman@gmail.com>
