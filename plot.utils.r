library(Rgraphviz)

my.plot.graph = function(g, labels) {
  nodes(g) = labels
  fontsize <- rep(1, (length(labels)))
  nAttrs <- list()
  nAttrs$width <-  rep(1, length(labels))
  names(nAttrs$width)<-labels
  nAttrs$height <- rep(1, length(labels))
  names(nAttrs$height)<-labels
  
  names(fontsize)<-labels
  x <- layoutGraph(g, nodeAttr=nAttrs)
  nodeRenderInfo(x) <- list(cex=fontsize)
  renderGraph(x) 
}

library(scales)
library(data.table)
library(ggplot2)
theme_set(theme_bw())
theme_update(legend.background = element_rect(colour="transparent", fill="transparent"))
output.dir = "../paper/figures/"
PLOT.WIDTH = 6
PLOT.HEIGHT = 4

# source: http://www.cookbook-r.com/Graphs/Multiple_graphs_on_one_page_(ggplot2)/
multiplot <- function(..., plotlist=NULL, file, cols=1, layout=NULL) {
  require(grid)
  
  # Make a list from the ... arguments and plotlist
  plots <- c(list(...), plotlist)
  
  numPlots = length(plots)
  
  # If layout is NULL, then use 'cols' to determine layout
  if (is.null(layout)) {
    # Make the panel
    # ncol: Number of columns of plots
    # nrow: Number of rows needed, calculated from # of cols
    layout <- matrix(seq(1, cols * ceiling(numPlots/cols)),
                     ncol = cols, nrow = ceiling(numPlots/cols))
  }
  
  if (numPlots==1) {
    print(plots[[1]])
    
  } else {
    # Set up the page
    grid.newpage()
    pushViewport(viewport(layout = grid.layout(nrow(layout), ncol(layout))))
    
    # Make each plot, in the correct location
    for (i in 1:numPlots) {
      # Get the i,j matrix positions of the regions that contain this subplot
      matchidx <- as.data.frame(which(layout == i, arr.ind = TRUE))
      
      print(plots[[i]], vp = viewport(layout.pos.row = matchidx$row,
                                      layout.pos.col = matchidx$col))
    }
  }
}
