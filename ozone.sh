Rscript ozone.data.r # preprocess data
matlab -nodesktop -nojvm -nosplash -r "addpath(genpath('/path/to/gpstuff/')); ozone, exit" # prewhiten data
Rscript ozone.r # run PC algorithm

