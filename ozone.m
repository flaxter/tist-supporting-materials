% Step 1: ozone.data.r loads the data, standardizes it (so each variable has mean 0 and variance 1), and saves it for matlab
% --> Step 2: ozone.m pre-whitens each variable
% Step 3: ozone.r runs the PC algorithm on the original data and the pre-whitened data and makes plots

load('ozone.mat'); 
% To run this script make sure GPstuff is in your path, e.g.:
% addpath(genpath('c:/users/seth/Downloads/gpstuff_matlab-4.5/'))

x = data.S;
[n, nin] = size(x);

fields = fieldnames(data);
whitened = zeros(n,numel(fields)-1);
for i = 2:numel(fields)
  y = data.(fields{i});
  lik = lik_gaussian('sigma2', 0.2^2,'sigma2_prior', prior_logunif());
  gpcf = gpcf_exp('lengthScale', 1.1, 'magnSigma2', 0.2^2,...
    'lengthScale_prior', prior_unif(), 'magnSigma2_prior', prior_sqrtunif()); 
  
  gp = gp_set('lik', lik, 'cf', gpcf);
  opt=optimset('TolFun',1e-3,'TolX',1e-3);
  gp=gp_optim(gp,x,y,'opt',opt);

  [w,s]=gp_pak(gp);  
  [Eft_map, Varft_map] = gp_pred(gp, x, y, x);
  
  whitened(:,(i-1)) = y - Eft_map;
end 

csvwrite('ozone_prewhitened.csv', whitened)

