# Step 1: generate data (ar1.r)
# Step 2: fit a GP with GPML in matlab (ar1.m)
# ---> Step 3: ar1.plots.r make plots and calculations for paper

library(R.matlab)
library(ggplot2)

source("utils.r")
source("plot.utils.r")
load("ar1.rdata")
gp = readMat("ar1.prewhitened.mat")
cor(gp$epsX,gp$epsY) # this number is in the paper

ymin = -10
ymax = 10
p.original = hsicTestGamma.wrapper(as.matrix(x),as.matrix(y)) # these numbers are in
p.prewhitened = hsicTestGamma.wrapper(gp$epsX,gp$epsY)        # the paper on page 13.

x = as.numeric(x)
y = as.numeric(y)

data1 = data.frame(time=seq(1.0,100.0,1), x=x, xhat=gp$Efx)
g = ggplot(data1, aes(x=time,y=x))
g = g + geom_line(aes(y=xhat), color="red")
g = g + geom_point(aes(y=x))
g = g + scale_y_continuous("X\n", limits=c(ymin,ymax))
g = g + scale_x_continuous("\ntime")
g = g + ggtitle("X over time")
g
p1 = g

data2 = data.frame(time=seq(1.0,100.0,1), x=y, xhat=gp$Efy)
g = ggplot(data2, aes(x=time,y=x))
g = g + geom_line(aes(y=xhat), color="blue")
g = g + geom_point(aes(y=x))
g = g + scale_y_continuous("Y\n", limits=c(ymin,ymax))
g = g + scale_x_continuous("\ntime")
g = g + ggtitle("Y over time")
g
p2 = g

data1$label = "X"
data2$label = "Y"
data0 = rbind(data1,data2)

data1$x = data1$x + 2
data1$xhat = data1$xhat + 2
data2$x = data2$x - 2
data2$xhat = data2$xhat - 2

data = rbind(data1,data2)
g = ggplot(data, aes(x=time,y=xhat, group=label))
g = g + geom_line(aes(y=xhat, color=label))

g = g + geom_point(aes(y=x)) #, color=label))
g = g + scale_y_continuous("", limits=c(ymin,ymax))
g = g + scale_colour_manual(values=c("red","blue"), guide=F)
g = g + scale_x_continuous("\ntime")
g = g + theme(legend.title=element_blank())
g = g + ggtitle("X versus Y over time")

g
p3 = g

data = data0
data$eps = data$x - data$xhat
g = ggplot(data, aes(x=time,y=eps, group=label))
g = g + geom_line(aes(y=eps, color=label))
g = g + scale_colour_manual(values=c("red","blue"), guide=F)
g = g + scale_x_continuous("\ntime")
g = g + scale_y_continuous("residual\n")
g = g + theme(legend.title=element_blank())
g = g + ggtitle("X residuals versus Y residuals over time")
g
p4 = g

data = data.frame(time=seq(1.0,100.0,1), x=x, y=y, epsx=x-gp$Efx, epsy=y-gp$Efy)
g = ggplot(data, aes(x=x, y=y))
g = g + geom_point()
g = g + scale_x_continuous("\nX")
g = g + scale_y_continuous("Y\n")
g = g + ggtitle(sprintf("X versus Y (p < %.01e)", p.original))
g
p5 = g

g = ggplot(data, aes(x=epsx, y=epsy))
g = g + geom_point()
g = g + scale_x_continuous("\nX residuals")
g = g + scale_y_continuous("Y residuals\n")
g = g + ggtitle(sprintf("X residuals versus Y residuals (p = %.02f)", p.prewhitened))
g
p6 = g

pdf("ar1-illustration.pdf", width=9, height=10)
multiplot(p3, p4, p5, p6, cols=2)
dev.off()
