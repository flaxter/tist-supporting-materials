# ---> Step 1: generate data (ar1.r)
# Step 2: fit a GP with GPML in matlab (ar1.m)
# Step 3: ar1.plots.r make plots and calculations for paper

library(R.matlab)
set.seed(96)
x = arima.sim(model=list(ar=c(.9)),n=100)
y = arima.sim(model=list(ar=c(.8)),n=100)

cor(x,y) # this number is in the paper on page 13
writeMat("ar1.mat", x=x, y=y)
save(x,y,file="ar1.rdata")
