% Step 1: generate data (ar1.r)
% ---> Step 2: fit a GP with GPML in matlab (ar1.m)
% Step 3: ar1.plots.r make plots and calculations for paper

% To run this script make sure GPML is in your path, e.g.:
% addpath(genpath('c:/users/seth/Downloads/gpml-matlab-v3.6-2015-07-07/'))

load('ar1.mat')

hyp.lik = log(0.1);
hyp.cov = log([0.5 1.0]);
hyp.mean = 0;
cov = @covSEiso; %, 5};
tt = linspace(0,100,100)';

hyp1 = minimize(hyp, @gp, -1000, @infExact, @meanConst, cov, @likGauss, tt, x);
[Efx, Varf] = gp(hyp1, @infExact, @meanConst, cov, @likGauss, tt, x, tt);
subplot(3,2,1);
plot(tt, x, 'black.', tt, Efx, 'r-'); title('X - observed and GP fit'); xlabel('Time'); ylabel('X');

hyp1 = minimize(hyp, @gp, -200, @infExact, @meanConst, cov, @likGauss, tt, y);
[Efy, Varf] = gp(hyp1, @infExact, @meanConst, cov, @likGauss, tt, y, tt);
subplot(3,2,2)
plot(tt, y, 'black.', tt, Efy, 'b-'); title('Y - observed and GP fit'); xlabel('Time'); ylabel('Y');

epsX = x - Efx;
epsY = y - Efy;

corr(x,y)
corr(epsX, epsY)

subplot(3,2,3)
plot(tt,Efx,'r-', tt,Efy,'b-'); title('Xhat and Yhat'); xlabel('Time'); ylabel('Observed value');

subplot(3,2,4)
plot(tt,epsX,'r-', tt,epsY,'b-'); title('X residuals and Y residuals'); xlabel('Time'); ylabel('Residual');

subplot(3,2,5)
plot(Efx,Efy,'.'); title('X vs. Y'); xlabel('X'); ylabel('Y');

subplot(3,2,6)
plot(epsX,epsY,'.'); title('X residuals vs. Y residuals'); xlabel('X - Xhat'); ylabel('Y - Yhat');

save('ar1.prewhitened.mat', 'epsX','epsY','Efx','Efy')
