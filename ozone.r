## Step 1: ozone.data.r loads the data, standardizes it (so each variable has mean 0 and variance 1), and saves it for matlab
## Step 2: ozone.m pre-whitens each variable
## --> Step 3: ozone.r runs the PC algorithm on the original data and the pre-whitened data and makes plots

library(pcalg)
source("utils.r")

# in the paper (Figure 7) we compare the PC algorithm run on the ozone data without prewhitening
# to the PC algorithm run on the ozone data with prewhitening.

# original dataset (well, the variables are actually standardized to have mean 0 and variance 1)
load("ozone.rdata")
alpha = .05
fit = pc(list(data=data), indepTest = check.cond.indep.PC, p = ncol(data), alpha=alpha, conservative=FALSE)
labels = names(data)
save(fit, alpha, labels, file="ozone_original_results.rdata")

pdf("ozone.pdf")
plot.graph(fit@graph, names(data))
dev.off()

# pre-whitened dataset (see ozone.m)
data = read.csv("ozone_prewhitened.csv",header=F,col.names=c("Ozone","Temp","InvHt","Pres","Vis","Hgt","Hum","InvTmp","Wind"))

fit = pc(list(data=data), indepTest = check.cond.indep.PC, p = ncol(data), alpha=alpha, conservative=FALSE)
labels = names(data)
save(fit, alpha, labels, file="ozone_prewhitened_results.rdata")

pdf("ozone-prewhitened.pdf")
plot.graph(fit@graph, names(data))
dev.off()
